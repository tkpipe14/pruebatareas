import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TareaService } from './tareas/tarea/tarea.service';
import { TareasController } from './tareas/tareas.controller';
import { tasks } from './tareas/tareas.entity';

@Module({
  imports: [
      TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'torres',
        database: 'tareas',
        entities: [tasks],
        synchronize: true,
      }),
      TypeOrmModule.forFeature([tasks]),

    ],
  controllers: [AppController, TareasController],
  providers: [AppService, TareaService],
})
export class AppModule {}
