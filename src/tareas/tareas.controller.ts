import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { Patch } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { HttpStatus } from '@nestjs/common/enums/http-status.enum';
import { TareaService } from './tarea/tarea.service';
import {tareasDto} from './tareasDto';

@Controller('tareas')
export class TareasController {

    constructor(private tareasservice: TareaService) { }

    @Get()
    ObtenerTareas() {
        return this.tareasservice.getTareas();
    }
    @Get(':id') 
    Obtenertarea(@Param() params) {
      return this.tareasservice.findOne(params);
    }

    @Post()
    createtarea(@Body() tarea: tareasDto) {
        this.tareasservice.createTarea(tarea);
    }

    @Patch(':id')
    async updatetarea(@Param('id') id: number, @Body() data: Partial<tareasDto>) {
      return {
        statusCode: HttpStatus.OK,
        message: 'tarea editada con exito',
        data: await this.tareasservice.updateTarea(id, data),
      };
    }

  
    @Delete(':id')
    deletetarea(@Param() params) {
      this.tareasservice.deleteTarea(params);
    }
}
