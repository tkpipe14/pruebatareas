    export interface tareasDto {
    id: number;
    nombre: string;
    descripcion: string;
    created_at: Date;
    update_at: Date;
}