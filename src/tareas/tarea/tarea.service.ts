import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Repository, DeleteResult } from 'typeorm';
import {tasks} from '../tareas.entity';
import {tareasDto} from '../tareasDto';
@Injectable()
export class TareaService {
    constructor(
        @InjectRepository(tasks)
        private tasksRepository: Repository<tasks>,
   ) {}


   tareas = {};
 
   getTareas (): Promise<tasks[]> {
        return this.tasksRepository.find();
   } 

    findOne(id: number): Promise<tasks> {
        return this.tasksRepository.findOne(id);
    }
    
    async createTarea(tareas): Promise<tasks> {
        return await this.tasksRepository.save(tareas);
    }
  
    
    async updateTarea(id: number, data: Partial<tasks>) {
        await this.tasksRepository.update({ id }, data);
        return await this.tasksRepository.findOne({ id });
    }

    async deleteTarea(id: number): Promise<void> {
        await this.tasksRepository.delete(id);
    }
     
    }

