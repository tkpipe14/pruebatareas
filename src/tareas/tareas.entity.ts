import {
    Entity,
    PrimaryGeneratedColumn,
    Column
  } from 'typeorm';
import { CreateDateColumn } from 'typeorm/decorator/columns/CreateDateColumn';
import { UpdateDateColumn } from 'typeorm/decorator/columns/UpdateDateColumn';
  
  @Entity()
  export class tasks {
      
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    nombre: string;

    @Column()
    descripcion: string;

    @CreateDateColumn()
    public created_at;

    @UpdateDateColumn()
    public updated_at;
  }